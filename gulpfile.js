var gulp = require('gulp');
var jade = require('gulp-jade');
var less = require('gulp-less');
var concat = require('gulp-concat');
var gulpAutoprefixer = require('gulp-autoprefixer');

gulp.task('jade', function() {
    gulp.src('./src/*.jade')
        .pipe(jade())
        .pipe(gulp.dest('./public/'))
});

gulp.task('less', function () {
    gulp.src('./src/styles/*.less')
        .pipe(less())
        .pipe(concat('style.css'))
        .pipe(gulpAutoprefixer('last 10 versions'))
        .pipe(gulp.dest('./public/styles/'))
});

gulp.task('watch', function() {
    gulp.watch('./src/styles/*.less', ['less']);
    gulp.watch('./src/*.jade', ['jade']);
});
